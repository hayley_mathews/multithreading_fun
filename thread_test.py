"""
experimenting with parallelism in python
"""

import random
import threading
import multiprocessing
import timeit


def list_append(count, id, output):
    """
    creates empty list and appends a random number count times
    CPU heavy operation
    """
    for x in range(count):
        output.append(random.random)

def threading_test(size, threads):
    jobs =  [threading.Thread(target=list_append, args=(size, x, list())) for x in range(0, threads)]
    [job.start() for job in jobs]
    [job.join() for job in jobs]


def multiprocessing_test(size, processes):
    jobs =  [multiprocessing.Process(target=list_append, args=(size, x, list())) for x in range(0, processes)]
    [job.start() for job in jobs]
    [job.join() for job in jobs]

if __name__ == '__main__':
    from timeit import Timer
    thread_timer = Timer("threading_test(1000000000, 4)", "from __main__ import threading_test")
    print("threading time:" )
    print(thread_timer.timeit(number=1))
    multiprocess_timer = Timer("multiprocessing_test(1000000000, 4)", "from __main__ import multiprocessing_test")
    print("multiprocessing time:")
    print(multiprocess_timer.timeit(number=1))
