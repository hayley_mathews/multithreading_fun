import random
import multiprocessing

def list_append(count, id, output):
    """
    creates empty list and appends a random number count times
    CPU heavy operation
    """
    for item in range(count):
        output.append(random.random)

if __name__ == "__main__":
    size = 10000000
    processes = 4

    jobs = []
    for item in range(0, processes):
        output = list()
        process = multiprocessing.Process(target=list_append, args=(size, item, output))
        jobs.append(process)

    for job in jobs:
        job.start()

    for job in jobs:
        job.join()
