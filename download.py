"""
learning about multithreading
"""

import json
import logging
import os
from pathlib import Path
from urllib.request import urlopen, Request

logger = logging.getLogger(__name__)

def get_links(client_id):
    headers = {'Authorization': 'Client-ID {}'.format(client_id)}
    request = Request('https://api.imgur.com/3/gallery/', headers=headers, method='GET')
    with urlopen(request) as response:
        data = json.loads(response.readall().decode('UTF-8'))
    return map(lambda item: item['link'], data['data'])

def download_link(directory, link):
    
